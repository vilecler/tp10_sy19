# tp10_sy19

TODO : https://docs.google.com/document/d/1wSO6EL0Y_WRC0GqIwETAtdfw14H9KojZteg1YhtsNKY/edit?usp=sharing

Exercice 1:

| Model | Score |
|---|---|
| Simple LDA | 0.0773 |
| Simple PDA | 0.0826 |
| Simple FDA (MARS) | 0.0773 |
| Simple FDA (BRUTO) | 0.076 |
| Simple RDA | 0.076 |
| Naive Bayes | 0.108 |
| Multinomial Logistic Regression | 0.296 |
| Simple classification Tree | 0.138 |
| Bagging Tree | 0.0773 |
| Random Forest | 0.076 |
| SVM | 0.0666 |
| NNET | 0.0813 |
| Deep Learning | 0.0773 |


LDA > Naive Bayes car Naive Bayes suppose que les données ne sont pas corrélées (matrice de covariance diagonanale) alors que ce n'est pas le cas.

https://sebastianraschka.com/faq/docs/large-num-features.html

Toute l'étude : https://www.r-bloggers.com/2018/11/linear-quadratic-and-regularized-discriminant-analysis/   http://statweb.lsu.edu/faculty/li/teach/exst7152/phoneme-example.html?fbclid=IwAR1Kd43TFZUk7UL_CIb5-_Vi6AqJqnWXrAEdlF_0sTaMJg-p5k8wpciTS7M

